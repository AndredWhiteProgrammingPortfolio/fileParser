/* Andre White
   
  
*/
#include <stdio.h>
#include <cstring>
#include <string>
#include <stdlib.h>

using namespace std;
int states[6][4]={ {0,1,2,5},{1,1,1,5},{2,5,2,3},{3,5,4,3},{4,5,4,5},{5,5,5,5}}; //state table
//Function to parse through strings to see if they are accepted or not. Only states 1, 2, and
//4 are accepting states and they will output the correct type of token it is. 
void parse(string input, FILE* stream){
	int state=0; //intial state
	for(int x=0;x<input.length();x++){
		//if input is a letter
		if(isalpha(input[x])){
			state=states[state][1];
		}
		//if input is a number
		else if(isdigit(input[x])){
			state=states[state][2];
		}
		//if input is a decimal
		else if(input[x]=='.'){
			state=states[state][3];
		}
		//if input is anything else. 
		else{
			state=5;
		}
	}
	//if the state is one of the accepting states then let us know. Invalid otherwise.
	switch(state){
		case 1:
			if((input=="int")||(input=="float")){
				fprintf(stream,"RESERVED WORD %15s\n",input.c_str());			}
			else
				fprintf(stream,"IDENTIFIER    %15s\n",input.c_str());
			break;
		case 2:
			fprintf(stream,"INTEGER       %15s\n", input.c_str());
			break;
		case 4:
			fprintf(stream,"REAL 	      %15s\n", input.c_str());
			break;
		default:
			fprintf(stream,"INVALID TOKEN %15s\n", input.c_str());
			break;
	}
}
//how to use
void usage(){

	fprintf(stdout, "Usage: parse filename1 [filename2],\n \twhere filename1 is the name of the input file to parse \n \tand filename2 is the optional output file.\n");

}
int main(int argc, char** argv){
	//check for the correct usage	
	if(argc<2){
		usage();
		exit(1);
	}
	FILE* inputFileHandle; //input handle for file
	FILE* outStream=stdout; //output handle which defaults to stdout
	char* file;		//file buffer
	if(argc>1){		
		inputFileHandle= fopen(argv[1], "r"); //open filename from argv for reading
		if(inputFileHandle==NULL){
			perror("Cannot open file");
		}
		fseek(inputFileHandle, 0, SEEK_END); //get the size of the file for buffer
		long size=ftell(inputFileHandle);
		rewind(inputFileHandle);	     //back to the beginning of handle
		file= new char[size];		     //create buffer
		fread(file,1,size-1,inputFileHandle); //fill buffer with file
		fclose(inputFileHandle);		//close file
	}	
	if (argc>2){
		outStream= fopen(argv[2], "w");		//if the output file is specified, open it
		if(outStream==NULL){
			perror("Cannot open file");
		}
	}
	char* token=strtok(file," ,;=\n");		//split file into tokens separated by special chars
	while(token!=NULL){
		parse(token,outStream);			//parse through all tokens
		token=strtok(NULL," ,;=\n");
	}
			
	if(argc==2){
		fclose(outStream);			//close file
	}
	return 0;
}


